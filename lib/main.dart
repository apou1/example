import 'package:flutter/material.dart';
import 'package:flutter_ble_uart/flutter_ble_uart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var provider = SerialConnectionProvider();

  void _startScanning(){
    var subscription = provider
      .scan(timeout: Duration(seconds: 5))
      .listen(_onDeviceFound, onDone: _onScanDone, cancelOnError: true);
  }
  void _onDeviceFound(result) {
    print(result);
    // TODO: Use ScanResult here, for instance to update the list of found devices.
  }
  void _onScanDone() {
    // This will be called when the scan is finished (timeout is reached)
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _startScanning,
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
